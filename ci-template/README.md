# Gitlab CI templates

You will find here documentation on how to use my gitlab CI templates.

There is 2 kind of templates

- [Job templates](./job) that are templates for a single job and might be extended in your CI to create fine-tuned pipelines.
- [Pipeline templates](./pipeline) that are less customizable but integrates together multiple jobs to create complex continuous integration quickly.

![industrialization image](./docs/images/industrialization.png)
