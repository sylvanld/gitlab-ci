# Gitlab Pages

## Mkdocs

Build and deploy static html documentation using mkdocs

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/job.yaml'

  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/pages/mkdocs.yaml'

pages:
  extends: .mkdocs_pages    
  rules:
    - if: $CI_COMMIT_TAG != null
      when: always
      variables:
        DOCUMENTATION_VERSION: $CI_COMMIT_TAG
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|MKDOCS_VERSION||no|1.2.3|
|MKDOCS_MATERIAL_VERSION||no|8.2.5|
|DOCUMENTATION_VERSION|Version of the documentation to be built. Will be available at <pages_url>/<DOCUMENTATION_VERSION>/|yes|-|
|GITLAB_CI_TOKEN|Token used to edit public/ branch where build documentation is stored **(available globally in `sylvanld` group)**|yes|-|
