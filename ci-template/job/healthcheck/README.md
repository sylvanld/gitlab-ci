
# Healthcheck Templates

## Healthcheck web service

Job that calls a given path and fails if received status code is unexpected.

Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/healthcheck/web_service.yaml'

healtcheck_api:
  extends: .healthcheck_web_service
  variables:
    HEALTHCHECK_URL: <service_url>
    SERVICE_NAME: my service
    EXPECTED_STATUS: 200
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|HEALTHCHECK_URL|URL of the service that is used to determine if it is working.|yes|-|
|SERVICE_NAME|A string used in logging message to indicates service status in a human readable way|yes|-|
|EXPECTED_STATUS|Status code that service must return when calling HEALTHCHECK_URL for healthcheck to be considered successful.|no|200|

